import matplotlib.pyplot as plt
import numpy as np
from sklearn.model_selection import train_test_split
from keras.layers import Dense,Flatten,Reshape
from keras.layers.advanced_activations import LeakyReLU
from keras.models import Sequential
from keras.optimizers import adam
import csv
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
np.set_printoptions(suppress=True)
import random
from pandas import read_csv
import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
def saveCSV(filename,mydata,how = "w"):
    with open(filename, how) as f:

        # using csv.writer method from CSV package
        write = csv.writer(f)

        #write.writerow(fields)
        write.writerows(mydata)
def makedir(namedir,path = "./"):
    # define the name of the directory to be created
    path=path+namedir

    try:
        os.mkdir(path)
    except OSError:
        print ("Creation of the directory %s failed" % path)
    else:
        print ("Successfully created the directory %s " % path)
img_rows=1
img_cols=31

img_shape = (img_rows,img_cols)

zdim=100
#Generator definition
def build_gen(img_shape,zdim):
    model = Sequential()
    model.add(Dense(31,input_dim=zdim))
    model.add(LeakyReLU(alpha=0.01))
    model.add(Dense(1*31,activation='tanh'))
    return model
#Discriminator definition
def build_dis(img_shape):
    model=Sequential()
    model.add(Flatten(input_shape=img_shape))
    model.add(Dense(31))
    model.add(LeakyReLU(alpha=0.01))
    model.add(Dense(1,activation='sigmoid'))
    return model

def build_gan(gen,dis):
    model = Sequential()
    model.add(gen)
    model.add(dis)
    return model
#Use the generator and discriminator functions for dis_v and gen_v
dis_v = build_dis(img_shape)
dis_v.compile(loss='binary_crossentropy',
              optimizer=adam(),
              metrics=['accuracy'])

gen_v = build_gen(img_shape,zdim)
gan_v = build_gan(gen_v,dis_v)
gan_v.compile(loss='binary_crossentropy',
              optimizer=adam()
              )

losses=[]
accuracies=[]
iteration_checks=[]


def shuffle_list(lst):
    lst2 = lst.copy()
    random.shuffle(lst2)
    return lst2
def train(iterations,batch_size,interval,Xtrainnew,trainPercent):

    #scaling each feature to 0 to 1 range.
    scaler = MinMaxScaler()
    scaled = scaler.fit_transform(Xtrainnew)
    Xtrain=scaled
    Xtrain = np.expand_dims(Xtrain,axis=1)

    real = np.ones((batch_size,1))
    fake = np.zeros((batch_size, 1))

    for iteration in range(iterations):
	#Generate random numbers
        ids = np.random.randint(0,Xtrain.shape[0],batch_size)
        imgs = Xtrain[ids]

        z=np.random.normal(0,1,(batch_size,100))
	#Generator
        gen_imgs = gen_v.predict(z)
        gen_imgs = np.expand_dims(gen_imgs,axis=1)
	#Discriminator
        dloss_real = dis_v.train_on_batch(imgs,real)
        dloss_fake = dis_v.train_on_batch(gen_imgs, fake)

        dloss,accuracy = 0.5 * np.add(dloss_real,dloss_fake)

        z = np.random.normal(0, 1, (batch_size, 100))
        gloss = gan_v.train_on_batch(z,real)

        if (iteration+1) % interval == 0:
            if interval<1000:
                interval=interval+100

            losses.append((dloss,gloss))
            accuracies.append(100.0*accuracy)
            iteration_checks.append(iteration+1)

            print("%d [D loss: %f , acc: %.2f] [G loss: %f]" %
                  (iteration+1,dloss,100.0*accuracy,gloss))
            show_data(gen_v,iteration+1,scaler,Xtrainnew,trainPercent)
#save to CSV file
def saveList2CSV(mynamefile,mylist):
    with open('./'+mynamefile, 'w') as myfile:
        wr = csv.writer(myfile, delimiter='\n', quoting=csv.QUOTE_MINIMAL)
        wr.writerow(mylist)
#export the synthetic data with different size 
def show_data(gen, iteration,scaler,Xtrainnew,trainPercent):
    z = np.random.normal(0, 1, (1000, 100))
    gen_imgs = gen.predict(z)
    unscaled=scaler.inverse_transform(gen_imgs)
    unscaled[:,0]=np.around(unscaled[:,0],0)
    mydata1=np.delete(unscaled, np.where(unscaled[:,0] == 0)[0], 0)
    mydata0=np.delete(unscaled, np.where(unscaled[:,0] == 1)[0], 0)
    prec=[0.25,0.50,1,1.25,1.5,1.75,2.0]
    for item in prec:
        number=item*len(Xtrainnew)
        number=number/2
        number=np.around(number,0)
        class1=mydata1[0:int(number)]
        class0=mydata0[0:int(number)]
        class01=np.concatenate((class0,class1),axis=0)
        np.savetxt("./"+str(trainPercent)+"/GAN_"+str(item)+"_"+str(iteration)+".csv", class01, delimiter=",")


trainpall ={0.05,0.06,0.07,0.08,0.09,0.1,0.2,0.3,0.4,0.5}
trainpall=sorted(trainpall)
# load real data
data = pd.read_csv('./wdbc.data',header=None)

X = data.iloc[:, 2:].values
ys = data.iloc[:, 1].values
y=np.ndarray(len(ys))
for index in range(0,len(ys)):
    if ys[index]== 'M':
        y[index]=1
    else:
        y[index]=0
# split real data to train and test part
while True:
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.40)
    if (abs(len(np.delete(y_train, np.where(y_train == 0)[0], 0))-len(np.delete(y_train, np.where(y_train == 1)[0], 0))))<(len(y_train)/7):
        break
dataframe_new=[]
for trainp in trainpall:

    mydata=np.column_stack((y_train,X_train))
    mytestdata=np.column_stack((y_test,X_test))
    makedir(str(trainp))
    saveCSV('./'+str(trainp)+'/train.csv',mydata)
    saveCSV('./'+str(trainp)+'/test.csv',mytestdata)

    url = './'+str(trainp)+'/train.csv'
    dataframe = read_csv(url, header=None, na_values='?')

    ## split the train part data to small part(size about trainp %)
    if len(dataframe_new)==0:
        while True:
            dataframe_all=dataframe.sample(frac = 1)
            dataframe_new=dataframe_all.iloc[0:round(len(dataframe)*trainp) , :]

            if(abs(len(np.delete(dataframe_new.values, np.where(dataframe_new.values[:, 0] == 0)[0], 0))-len(np.delete(dataframe_new.values, np.where(dataframe_new.values[:, 0] == 1)[0], 0))))<(len(dataframe_new)/8):
                dataframe_all=dataframe_all.iloc[round(len(dataframe)*trainp):len(dataframe_all) , :]
                break
    else:
        need=round(len(dataframe)*trainp)-len(dataframe_new)
        dataframe_all=dataframe_all.sample(frac = 1)
        new_data=dataframe_all.iloc[0:need , :]
        dataframe_new=pd.concat([dataframe_new,new_data])
        dataframe_all=dataframe_all.iloc[need:len(dataframe_all) , :]
    dataframe_new.to_csv('./'+str(trainp)+'/train'+str(trainp)+'.csv', encoding='utf-8',header=False)
    batch_size=128
    #Call the GAN function (10000 is maximum Epoch,100 is minimum Epoch)
    train(10000,batch_size,100,dataframe_new,trainp)
