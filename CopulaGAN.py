# import library
from sdv.tabular import CopulaGAN
import pandas as pd


def calculate_synthetic_data_copulagan(data: pd.df, epoch: int, rowfactor=1) -> object:
    """
    @param rowfactor: integer of number of rows created by the
    @rtype: a dataframe of the synthetic data
    @param data: panda dataframe with column name of the training data for the CopulaGAN method
    @param epoch: an integer with the number of epochs the model should train
    @return: a dataframe of the synthetic data of CopulaGAN
    """
    # create the CopulaGAN model with a specific number of epochs
    model = CopulaGAN(epochs=epoch)
    # train the model with the input data
    model.fit(data)
    # get the number rows of the training data
    number_synthetic_data = rowfactor * data.shape[0]
    # create synthetic data
    synthetic_data = model.sample(number_synthetic_data)
    return synthetic_data


def write_synthetic_data(synthetic_data_df: pd.df, output_path: str, columnnames: list) -> None:
    """

    @rtype: None
    @param synthetic_data_df: a pandas dataframe of the synthetic data
    @param output_path: a string of the ouput path
    @param columnnames: a list of the column names of the synthetic data
    """
    # be sure that the synthetic data is a pandas dataframe with column names
    df = pd.DataFrame(synthetic_data_df, columns=columnnames)
    # write the dataframe to the output path
    df.to_csv(output_path)


# the path of the input data
data_path = "test_data.csv"
# declare the iteration numbers
epoch_number = 300
# define the header of the output data
header = ['diagnosis', 'radius_mean', 'texture_mean', 'perimeter_mean', 'area_mean', 'smoothness_mean',
          'compactness_mean', 'concavity_mean', 'concave points_mean', 'symmetry_mean', 'fractal_dimension_mean',
          'radius_se', 'texture_se', 'perimeter_se', 'area_se', 'smoothness_se', 'compactness_se', 'concavity_se',
          'concave points_se', 'symmetry_se', 'fractal_dimension_se', 'radius_worst', 'texture_worst',
          'perimeter_worst', 'area_worst', 'smoothness_worst', 'compactness_worst', 'concavity_worst',
          'concave points_worst', 'symmetry_worst', 'fractal_dimension_worst']

# read in the data as a dataframe with a specif column names
training_data = pd.read_csv(data_path, header=None, names=header, sep=",")

# create the synthetic data with the training data and the number of epochs of 300
synthetic_data_copula = calculate_synthetic_data_copulagan(data=training_data, epoch=epoch_number)

# write the results down to an output path
write_synthetic_data(synthetic_data_df=synthetic_data_copula, output_path="synthetic_data_CopulaGAN.csv",
                     columnnames=header)
