import matplotlib.pyplot as plt
import numpy as np

from keras.layers import Dense,Flatten,Reshape,Embedding,Multiply,Concatenate,Input
from keras.layers.advanced_activations import LeakyReLU

from keras.models import Model,Sequential
from keras.optimizers import adam
from sklearn.model_selection import train_test_split
import csv
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
np.set_printoptions(suppress=True)
import random
import os
from pandas import read_csv
def saveCSV(filename,mydata,how = "w"):
    with open(filename, how) as f:

        # using csv.writer method from CSV package
        write = csv.writer(f)

        write.writerows(mydata)
def makedir(namedir,path = "./"):
    # define the name of the directory to be created
    path=path+namedir

    try:
        os.mkdir(path)
    except OSError:
        print ("Creation of the directory %s failed" % path)
    else:
        print ("Successfully created the directory %s " % path)
img_rows=1
img_cols=31

img_shape = (img_rows,img_cols)

zdim=100
num_classes=2
#Generator definition
def build_gen(img_shape,zdim):
    model = Sequential()
    model.trainable = True
    model.add(Dense(31,input_dim=zdim))
    model.add(LeakyReLU(alpha=0.01))
    model.add(Dense(1*31,activation='tanh'))
    return model
def build_cgen(img_shape,zdim):
    z=Input(shape=(zdim,))
    lable=Input(shape=(1,),dtype='int32')
    lable_emb= Embedding(num_classes,zdim,input_length=1)(lable)
    lable_emb=Flatten()(lable_emb)
    joined_rep = Multiply()([z,lable_emb])
    gen_v=build_gen(img_shape,zdim)
    c_img=gen_v(joined_rep)
    return Model([z,lable],c_img)
#Discriminator definition
def build_dis(img_shape):
    model=Sequential()
    model.trainable = True
    model.add(Flatten(input_shape=img_shape))
    model.add(Dense(31))
    model.add(LeakyReLU(alpha=0.01))
    model.add(Dense(1,activation='sigmoid'))
    return model

def build_cdis(img_shape):
    img=Input(shape=(img_cols,))
    lable=Input(shape=(1,),dtype='int32')
    lable_emb= Embedding(num_classes,np.prod((31)),input_length=1)(lable)
    lable_emb=Flatten()(lable_emb)
    concate_img=Concatenate(axis=-1)([img,  lable_emb])

    dis_v= build_dis((img_rows,img_cols*2))
    classification=dis_v(concate_img)
    return Model([img,lable],classification)
def build_cgan(genrator,discriminator):
    z=Input(shape=(zdim,))
    lable=Input(shape=(1,),dtype='int32')
    f_img=genrator([z,lable])
    classification= discriminator([f_img,lable])
    model=Model([z,lable],classification)
    return model
#Use the generator and discriminator functions for dis_v and gen_v
dis_v = build_cdis(img_shape)
dis_v.compile(loss='binary_crossentropy',
              optimizer=adam(),
              metrics=['accuracy'])

gen_v = build_cgen(img_shape,zdim)
dis_v.trainable=False
gan_v = build_cgan(gen_v,dis_v)
gan_v.compile(loss='binary_crossentropy',
              optimizer=adam()
              )

losses=[]
accuracies=[]
iteration_checks=[]


def shuffle_list(lst):
    lst2 = lst.copy()
    random.shuffle(lst2)
    return lst2
def train(iterations,batch_size,interval,Xtrainnew,trainPercent):


    ytrain=[]
    for j in Xtrainnew.values:
        ytrain.append(j[0])

    Ytrainnew=np.array(ytrain)
    #scaling each feature to 0 to 1 range.
    scaler = MinMaxScaler()
    scaled = scaler.fit_transform(Xtrainnew)
    Xtrain=scaled
    ytrain=Ytrainnew


    real = np.ones((batch_size,1))
    fake = np.zeros((batch_size, 1))

    for iteration in range(iterations):
	#Generate random numbers
        ids = np.random.randint(0,Xtrain.shape[0],batch_size)
        imgs = Xtrain[ids]
        labels = ytrain[ids]
	#Generator
        z=np.random.normal(0,1,(batch_size,100))
        gen_imgs = gen_v.predict([z,labels])
	#Discriminator
        dloss_real = dis_v.train_on_batch([imgs,labels],real)
        dloss_fake = dis_v.train_on_batch([gen_imgs,labels], fake)

        dloss,accuracy = 0.5 * np.add(dloss_real,dloss_fake)


        z = np.random.normal(0, 1, (batch_size, 100))
        labels=np.random.randint(0,num_classes,batch_size).reshape(-1,1)
        gloss = gan_v.train_on_batch([z,labels],real)

        if (iteration+1) % interval == 0:
            if interval<1000:
                interval=interval+100
            losses.append((dloss,gloss))
            accuracies.append(100.0*accuracy)
            iteration_checks.append(iteration+1)

            print("%d [D loss: %f , acc: %.2f] [G loss: %f]" %
                  (iteration+1,dloss,100.0*accuracy,gloss))
            show_data(gen_v,iteration+1,scaler,Xtrainnew,trainPercent)
#save to CSV file
def saveList2CSV(mynamefile,mylist):
    with open('./'+mynamefile, 'w') as myfile:
        wr = csv.writer(myfile, delimiter='\n', quoting=csv.QUOTE_MINIMAL)
        wr.writerow(mylist)
#export the synthetic data with different size 
def show_data(gen, iteration,scaler,Xtrainnew,trainPercent):
    z = np.random.normal(0, 1, (1000, 100))
    labels=np.random.randint(2, size=1000)
    gen_imgs = gen.predict([z,labels])
    unscaled=scaler.inverse_transform(gen_imgs)
    unscaled[:,0]=np.around(unscaled[:,0],0)
    mydata1=np.delete(unscaled, np.where(unscaled[:,0] == 0)[0], 0)
    mydata0=np.delete(unscaled, np.where(unscaled[:,0] == 1)[0], 0)
    prec=[0.25,0.50,1,1.25,1.5,1.75,2.0]
    for item in prec:
        number=item*len(Xtrainnew)
        number=number/2
        number=np.around(number,0)
        class1=mydata1[0:int(number)]
        class0=mydata0[0:int(number)]
        class01=np.concatenate((class0,class1),axis=0)
        np.savetxt("./"+str(trainPercent)+"/CGAN_"+str(item)+"_"+str(iteration)+".csv", class01, delimiter=",")
trainpall ={0.05,0.06,0.07,0.08,0.09,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9}

for trainp in trainpall:
    # load real data
    dataframe_new = pd.read_csv('./'+str(trainp)+'/train'+str(trainp)+'.csv',header=None)
    dataframe_new=dataframe_new.iloc[:, 1:]
    batch_size=128
    train(10000,batch_size,100,dataframe_new,trainp)
