# import libraries
import numpy as np
import pandas as pd
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import MinMaxScaler, OneHotEncoder
from sklearn.impute import SimpleImputer
from sklearn.compose import ColumnTransformer

# import modules from the WGANGP repository
from WGANGP.helpers import get_cat_dims
from WGANGP.models import WGANGP

# this script is an adaption of the wisonsin breast cancer data to the WGANGP method of Engelmann, J.; Lessmann, S.
# Conditional Wasserstein GAN-based Oversampling of Tabular Data for Imbalanced Learning.

# declaration of the input path and the iteration number
path_training_data = "test_data.csv"
epoch_number = 300

# set the numeric and categorical columns
# in this dataset there are no categorial columns but the id column is used as one because without this input the
# algorithm creates an error
cat_cols = ('id')
num_cols = ['radius_mean', 'texture_mean', 'perimeter_mean', 'area_mean', 'smoothness_mean',
                        'compactness_mean', 'concavity_mean', 'concave points_mean', 'symmetry_mean', 'fractal_dimension_mean',
                        'radius_se', 'texture_se', 'perimeter_se', 'area_se', 'smoothness_se', 'compactness_se', 'concavity_se',
                        'concave points_se', 'symmetry_se', 'fractal_dimension_se', 'radius_worst', 'texture_worst',
                        'perimeter_worst', 'area_worst', 'smoothness_worst', 'compactness_worst', 'concavity_worst',
                        'concave points_worst', 'symmetry_worst', 'fractal_dimension_worst']

# create a list of strings for the header of the input data
head = ['id','diagnosis','radius_mean', 'texture_mean', 'perimeter_mean', 'area_mean', 'smoothness_mean',
                        'compactness_mean', 'concavity_mean', 'concave points_mean', 'symmetry_mean', 'fractal_dimension_mean',
                        'radius_se', 'texture_se', 'perimeter_se', 'area_se', 'smoothness_se', 'compactness_se', 'concavity_se',
                        'concave points_se', 'symmetry_se', 'fractal_dimension_se', 'radius_worst', 'texture_worst',
                        'perimeter_worst', 'area_worst', 'smoothness_worst', 'compactness_worst', 'concavity_worst',
                        'concave points_worst', 'symmetry_worst', 'fractal_dimension_worst']

# select the name of the label column
target_col = 'diagnosis'

# read the training data for the WGANGP
df_train = pd.read_csv(path_training_data, sep=',', index_col=None,
                             dtype={col: 'category' for col in cat_cols})
# set the header
df_train.columns = head
# get the list of the categorical column names
cat_cols_list = ["id"]

# split the training data into X and Y part
X_train = df_train.loc[:, num_cols + cat_cols_list]
y_train = df_train.loc[:, target_col]

# get the dimension or the position of the categorical value swithin the dataframe
cat_dims = get_cat_dims(X_train, ["id"])

# preprocess data wit two different scalers for numerical and categorical data
num_prep = make_pipeline(SimpleImputer(strategy='mean'),
                                     MinMaxScaler())
cat_prep = make_pipeline(SimpleImputer(strategy='most_frequent'),
                                     OneHotEncoder(handle_unknown='ignore', sparse=False))
prep = ColumnTransformer([
                ('num', num_prep, num_cols),
                ('cat', cat_prep, cat_cols_list)],
                remainder='drop'
            )
X_train_trans = prep.fit_transform(X_train)

# setup the WGANGP model with different options
gan = WGANGP(write_to_disk=True, # whether to create an output folder. Plotting will be surpressed if flase
                        compute_metrics_every=1250, print_every=2500, plot_every=10000,
                        num_cols = num_cols, cat_dims=cat_dims,
                        # pass the one hot encoder to the GAN to enable count plots of categorical variables
                        transformer=prep.named_transformers_['cat']['onehotencoder'],
                        # pass column names to enable
                        cat_cols=cat_cols,
                        use_aux_classifier_loss=True,
                        d_updates_per_g=3, gp_weight=15)

# fit the data to the model
gan.fit(X_train_trans, y=y_train.values,
                    condition=True,
                    epochs=epoch_number,
                    batch_size=64,
                    netG_kwargs = {'hidden_layer_sizes': (128,64),
                                    'n_cross_layers': 1,
                                    'cat_activation': 'gumbel_softmax',
                                    'num_activation': 'none',
                                    'condition_num_on_cat': True,
                                    'noise_dim': 30,
                                    'normal_noise': False,
                                    'activation':  'leaky_relu',
                                    'reduce_cat_dim': True,
                                    'use_num_hidden_layer': True,
                                    'layer_norm':False,},
                    netD_kwargs = {'hidden_layer_sizes': (128,64,32),
                                    'n_cross_layers': 2,
                                    'embedding_dims': 'auto',
                                    'activation':  'leaky_relu',
                                    'sigmoid_activation': False,
                                    'noisy_num_cols': True,
                                    'layer_norm':True}
                   )

# this method you can use to extend the minority class in your data
# X_res, y_res = gan.resample(X_train_trans, y_train)

# here we generate the synthetic data
X = gan.sample(n=y_train.shape[0])

# split the labels from the synthetic data
X_res, y_res = np.hsplit(X, [-2])

# only use the second column for the labels (first label was only for not creating an error by generate the model)
y_res = y_res[:, 1]

# delete the additational information provide by the algorithm from 31 column
numeric_features_array = np.delete(X_res, range(30,X_res.shape[1]),1)
# transfer the data back to its original data
numeric_scaler = prep.named_transformers_['num'][1]
inv_Fake_data = numeric_scaler.inverse_transform(numeric_features_array)

# create a data frame of the data
df_features = pd.DataFrame(inv_Fake_data, columns=['radius_mean', 'texture_mean', 'perimeter_mean', 'area_mean', 'smoothness_mean',
                        'compactness_mean', 'concavity_mean', 'concave points_mean', 'symmetry_mean', 'fractal_dimension_mean',
                        'radius_se', 'texture_se', 'perimeter_se', 'area_se', 'smoothness_se', 'compactness_se', 'concavity_se',
                        'concave points_se', 'symmetry_se', 'fractal_dimension_se', 'radius_worst', 'texture_worst',
                        'perimeter_worst', 'area_worst', 'smoothness_worst', 'compactness_worst', 'concavity_worst',
                        'concave points_worst', 'symmetry_worst', 'fractal_dimension_worst'])
# create dataframe with the labels
df_label = pd.DataFrame(y_res, columns=['diagnosis'])

# combine the dataframe of labels and features
df_complete_fake = pd.concat([df_label, df_features], axis=1)
# write the synthetic data down
df_complete_fake.to_csv("synthetic_data_WGANGP.csv")
