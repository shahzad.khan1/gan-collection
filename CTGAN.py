from pandas import read_csv
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
import csv
import os
from sdv.tabular import CTGAN
import sys
def saveCSV(filename,mydata,how = "w"):
    with open(filename, how) as f:

        # using csv.writer method from CSV package
        write = csv.writer(f)

        write.writerows(mydata)
def makedir(namedir,path = "./"):
    # define the name of the directory to be created
    path=path+namedir

    try:
        os.mkdir(path)
    except OSError:
        print ("Creation of the directory %s failed" % path)
    else:
        print ("Successfully created the directory %s " % path)
def lable_balance(mydata):

    if (abs(len(np.delete(mydata.values, np.where(mydata.values[:, 0] == 'B')[0], 0))-len(np.delete(mydata.values, np.where(mydata.values[:, 0] == 'M')[0], 0))))<(len(mydata)/8):
        #break
        return True
    return False
#the main CTGAN function and export the synthetic data with different size 
def CTGAN_main(dataframe_new,trainPercent):
    iteration=100
    while(iteration<=10000):
        model = CTGAN( epochs=iteration)
        dataframe_new.columns = dataframe_new.columns.map(str)
        model.fit(dataframe_new)
        synthetic=model.sample(1000)
        synthetic=synthetic.to_numpy()
        mydata1=np.delete(synthetic, np.where(synthetic[:,0] == 0)[0], 0)
        mydata0=np.delete(synthetic, np.where(synthetic[:,0] == 1)[0], 0)
        prec=[0.25,0.50,1,1.25,1.5,1.75,2.0]
        for item in prec:
            number=item*len(dataframe_new)
            number=number/2
            number=np.around(number,0)
            class1=mydata1[0:int(number)]
            class0=mydata0[0:int(number)]
            class01=np.concatenate((class0,class1),axis=0)
            np.savetxt("./"+str(trainPercent)+"/CTGAN_"+str(item)+"_"+str(iteration)+".csv", class01, delimiter=",")
        if iteration<1000:
            iteration=iteration+100
        else:
            iteration=iteration+1000

trainpall ={0.05,0.06,0.07,0.08,0.09,0.1,0.2,0.3,0.4,0.5}

for trainp in trainpall:
    # load real data
    dataframe_new = pd.read_csv('./'+str(trainp)+'/train'+str(trainp)+'.csv',header=None)
    dataframe_new=dataframe_new.iloc[:, 1:]
    #Call the CTGAN function (10000 is maximum Epoch,100 is minimum Epoch)
    CTGAN_main(dataframe_new,trainp)

